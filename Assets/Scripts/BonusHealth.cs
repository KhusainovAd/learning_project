﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusHealth : MonoBehaviour
{
    [SerializeField] private int amountOfBonusHealth;
    
    [SerializeField] private Animator animator;
    public int AmountOfBonusHealth
    {
        get {return amountOfBonusHealth;}
        set 
        {   
            if (value > 30)
            amountOfBonusHealth = value;
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other) {
        Health health = other.gameObject.GetComponent<Health>();
        if (health != null) {
        health.SetHealth(amountOfBonusHealth);
        StartDestroy();
        }
    }

    public void StartDestroy() {
        animator.SetTrigger("MedkitDestroy");
    }
    public void EndDestroy() {
        Destroy(gameObject);
    }
}
