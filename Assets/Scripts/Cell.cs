﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Cell : MonoBehaviour
{
    [SerializeField] private Image icon;
    private Item item;
    private void Awake() {
        icon.sprite = null;    
    }
    public void Init(Item item)
    {
        this.item = item;
        icon.sprite = item.Icon;
    }
    public void Init()
    {
        this.item = null;
        icon.sprite = null;
    }
    public Action OnButtonPressed;
    public void OnClickCell() 
    //Нужно чтобы Cell при нажатии вызывал метод InventoryUpdate у InventoryUIController
    {

        if (item == null)
            return;
        GameManager.Instance.inventory.Items.Remove(item); //Не рабоает
        Buff buff = new Buff //Создаем бафф для списка по клику
        {
            type = item.Type,
            additiveBonus = item.Value
        };
        GameManager.Instance.inventory.buffReciever.AddBuff(buff); 
        if(OnButtonPressed != null)
            OnButtonPressed();
        //Добавляем бафф в бафресивер
    }
}
