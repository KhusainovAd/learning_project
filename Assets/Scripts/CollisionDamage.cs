﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDamage : MonoBehaviour
{
    [SerializeField] private float damage = 10;
    [SerializeField] private Animator animator;
    private IObjectDestroyer destroyer;
    [SerializeField] private GameObject player;
    private float armorBonus;
    public float Damage
    {
        get {return damage;}
        set {
            if (value > 0 )
            damage = value;
        }
    }
    private float direction;
    private Health health;
    public float Direction {
        get {return direction;}
    }
    
    private void OnCollisionStay2D(Collision2D other) 
    {
            if (GameManager.Instance.healthContainter.ContainsKey(other.gameObject))
            {   
                health = GameManager.Instance.healthContainter[other.gameObject];
                direction = (other.transform.position - transform.position).x;
                animator.SetFloat("Direction", Mathf.Abs(direction));
                if (other.gameObject == player)
                {
                    player.GetComponent<Animator>().SetTrigger("DmgtoPlayer");
                }
            }
    }

    public void SetDamage()
    {
        GameObject player = GameObject.FindWithTag("Player");//Находим плеера
        armorBonus = player.GetComponent<Player>().ArmorBonus;//Вытаскиваем доп урон
        if (health !=null)
            health.TakeHit((int)(damage-armorBonus));
        health = null;
        direction = 0;
        animator.SetFloat("Direction", 0f);
    }
}
