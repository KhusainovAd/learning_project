﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    [SerializeField] private GameObject leftBorder;
    [SerializeField] private GameObject rightBorder;
    [SerializeField] private Rigidbody2D rigidbody;
    [SerializeField] private GroundDetection groundDetection;

    [SerializeField] private bool isRightDirection;
    public bool IsRightDirection
    {
        get {return isRightDirection;}
        set {isRightDirection = value;}
    }
    [SerializeField] private float speed;
    public float Speed
    {
        get {return speed;}
        set {speed = value;}
    }
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Animator animator;
    [SerializeField] private CollisionDamage collisionDamage;
    [SerializeField] private GameObject player;

    private void FixedUpdate() {
        if (groundDetection.IsGrounded)
        {
            if (transform.position.x > rightBorder.transform.position.x
                || collisionDamage.Direction < 0)
                isRightDirection = false;
            else if (transform.position.x < leftBorder.transform.position.x
                || collisionDamage.Direction > 0)
                isRightDirection = true;
            rigidbody.velocity = isRightDirection ? Vector2.right : Vector2.left;
            rigidbody.velocity *= speed;
        }
        if (rigidbody.velocity.x > 0)
            spriteRenderer.flipX = true;
        if (rigidbody.velocity.x < 0)
            spriteRenderer.flipX = false;

    }

}
