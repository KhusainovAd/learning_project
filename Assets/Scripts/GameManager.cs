﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject inventoryPanel;
    public static GameManager Instance {get; private set;}
    public Dictionary<GameObject, Health> healthContainter;
    public Dictionary<GameObject, Coin> coinContainer;
    public static string previousScene;
    public Dictionary<GameObject, ItemComponent> itemsContainer;
    public ItemBase itemDataBase;
    [HideInInspector] public PlayerInventory inventory;

    public Dictionary<GameObject, BuffReciever> buffRecieverContainer;
    private void Awake() 
    {
        Instance = this;
        healthContainter = new Dictionary<GameObject, Health>();
        coinContainer = new Dictionary<GameObject, Coin>();
        buffRecieverContainer = new Dictionary<GameObject, BuffReciever>();
        itemsContainer = new Dictionary<GameObject, ItemComponent>();
    }

    public void OnClickPause()
    {   
        previousScene = SceneManager.GetActiveScene().name;
        if (Time.timeScale>0)
        {
        inventoryPanel.gameObject.SetActive(true);
        Time.timeScale = 0;
        //SceneManager.LoadScene(2, LoadSceneMode.Additive);
        }
        else 
        {
        inventoryPanel.gameObject.SetActive(false);
        Time.timeScale = 1;
        }
    }

}
