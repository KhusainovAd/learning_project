﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetection : MonoBehaviour
{
    [SerializeField] private bool isGrounded;
    public bool IsGrounded
{
    get {return isGrounded;}
    set {isGrounded = value;}
}

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Square")){
            isGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other) {
        if (other.gameObject.CompareTag("Square")){
            isGrounded = false;
        }    
    }
}
