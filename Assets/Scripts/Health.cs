﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private int health;
    public int CurrentHealth
    {
        get { return health;}
    }

    private void Start() {
        GameManager.Instance.healthContainter.Add(gameObject,this);
    }
    public int Health_
    {
        get {return health;}
        set {health = value;}
    }
    public void TakeHit(int damage)
    {
        health -= damage;
        Debug.Log(health);
        if (health <=0)
            Destroy(gameObject);
    }

    public void SetHealth(int bonusHealth)
    {
        health += bonusHealth;
        if (health > 100)
            health = 100;
    }
}
