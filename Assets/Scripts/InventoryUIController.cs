﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InventoryUIController : MonoBehaviour
{
    [SerializeField] Cell[] cells;
    [SerializeField] private int cellCount;
    [SerializeField] private Cell cellPrefab;
    [SerializeField] private Transform rootParent;

    void Init()
    {
        Debug.Log("Инициализация");
        cells = new Cell[cellCount];
        for (int i = 0; i < cellCount; i++)
        {
            cells[i] = Instantiate(cellPrefab, rootParent);
            cells[i].OnButtonPressed += InvetoryUpdate;
        }
        cellPrefab.gameObject.SetActive(false);
      //  cellPrefab.OnButtonPressed += InvetoryUpdate;
    }

    private void OnEnable()
    {
        if (cells == null || cells.Length <=0)
            Init();
        InvetoryUpdate();

    }
    private void InvetoryUpdate()
    {
        var inventory = GameManager.Instance.inventory;
        for (int i = 0; i < cells.Length; i++)
        {
            if (i < inventory.Items.Count)
                cells[i].Init(inventory.Items[i]);
            else
                cells[i].Init();
        }
    }
}
