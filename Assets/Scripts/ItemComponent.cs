﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemComponent : MonoBehaviour
{
    [SerializeField] private ItemType type;
    private Item item;
    public Item Item
    { get {return item;}
    set {item = value;}
    }
    [SerializeField] private SpriteRenderer spriteRenderer;
    public void Destroy(GameObject gameObject)
    {
        MonoBehaviour.Destroy(gameObject);
    }
    void Start()
    {
        item = GameManager.Instance.itemDataBase.GetItemOfID((int)type);
        spriteRenderer.sprite = item.Icon;
        GameManager.Instance.itemsContainer.Add(gameObject, this);
    }

}

public enum ItemType
{
    ForcePotion = 3, 
    DamagePotion = 1, 
    ArmorPotion = 2
}
