﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    private int isSoundOn = 0;
    [SerializeField] private GameObject soundOffimage;
    [SerializeField] private GameObject soundOnimage;

    private void Start()
    {
        isSoundOn = PlayerPrefs.GetInt("isSoundOn", isSoundOn);
        bool initialSoundState = isSoundOn == 1 ? true : false;
        soundOffimage.SetActive(!initialSoundState);
        soundOnimage.SetActive(initialSoundState);
    }
    public void ContinuePlay()
    {
        SceneManager.UnloadSceneAsync(2);
        Time.timeScale = 1;
    }
    public void TurnSound()
    {
        if (isSoundOn == 0)
        {
            isSoundOn = 1;
            PlayerPrefs.SetInt("isSoundOn", isSoundOn);
            soundOffimage.SetActive(false);
            soundOnimage.SetActive(true);
        }
        else if (isSoundOn == 1)
        {   
            isSoundOn = 0;
            PlayerPrefs.SetInt("isSoundOn", isSoundOn);
            soundOffimage.SetActive(true);
            soundOnimage.SetActive(false);
        }
    }
    public void goToMainMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }
    public void exitGame()
    {
        Application.Quit();
    }
}
