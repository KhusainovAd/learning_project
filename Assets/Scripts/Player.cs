﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Player : MonoBehaviour
{
[SerializeField] private float speed;
public float Speed
{
    get {return speed;}
    set {
        if (value > 0.5)
        speed = value;
    }
}
[SerializeField] private float force;
[SerializeField] private Rigidbody2D rigidbody;
[SerializeField] private float minimalHeight;
public float MinimalHeight
{
    get {return minimalHeight;}
    set {
        if (value > -100)
        minimalHeight = value;
    }
}
[SerializeField] private bool isCheatMode;
private Vector3 direction;
[SerializeField] private GroundDetection groundDetection;
[SerializeField] private Animator animator;
[SerializeField] private SpriteRenderer spriteRenderer;

[SerializeField] private bool isJumping;
[SerializeField] private Arrow arrow;
[SerializeField] private Transform arrowSpawnPoint;
[SerializeField] private float shootForce = 5f;
private Arrow currentArrow;
private List<Arrow> arrowPool; 

public float ShootForce
{
    get {return shootForce;}
    set {shootForce = value;}
}

[SerializeField] private bool isReloading;
[SerializeField] private float reloadingTime;
[SerializeField] private int arrowsCount = 3;
[SerializeField] private BuffReciever buffReciever; //Мы передаем бафресивер со списком бафов
[SerializeField] private float forceBonus;
[SerializeField] private float armorBonus;
public float ArmorBonus
{get {return armorBonus;}}
[SerializeField] private float damageBonus;
public float DamageBonus
{get {return damageBonus;}}
private List<Buff> buffs;
public float ReloadingTime
{
    get {return reloadingTime;}
    set {reloadingTime = value;}
}
public bool IsJumping
{
    get {return isJumping;}
    set {isJumping = value;}
}
public static Player Instance {get; set;} //= null;
[SerializeField] private Health health;
public Health Health {get { return health;}}
private UICharacterController controller;
private void Awake() {
    Instance = this;
}
private void Start()
{
    arrowPool = new List<Arrow>();
    for (int i = 0; i < arrowsCount; i++)
    {
        var arrowTemp =  Instantiate(arrow, arrowSpawnPoint);
            arrowPool.Add(arrowTemp);
            arrowTemp.gameObject.SetActive(false);
    }

    buffReciever.OnBuffsChanged +=  UsingBuff;
}

public void InitUIController(UICharacterController uiController)
{
    controller = uiController;
    controller.Jump.onClick.AddListener(Jump);
    controller.Fire.onClick.AddListener(CheckShoot);
}
    private void UsingBuff()
{
    //Надо пройтись по списку использованных бафов и 
    // достать из него buff.additiveBonus в зависимости от buff.type
    buffs = buffReciever.Buffs; //Передаю ссылку на список бафов

    foreach (Buff buff in buffs){
        //Нужно передать сюда как-то ItemType из базы для сравнения
        //buff.type == GameManager.Instance.itemDataBase.GetItemOfID((int)3).GetType()
        /*if (buff.type.ToString() == "Force") {
            forceBonus = buff.additiveBonus;
        //    buffs.Remove(buff);
        }
        if (buff.type.ToString() == "Damage") {
            damageBonus = buff.additiveBonus;
        }
        if (buff.type.ToString() == "Armor") {
            armorBonus = buff.additiveBonus;
        }*/
        switch (buff.type)
        {
            case BuffType.Damage:
                damageBonus =  buff.additiveBonus;
                break;
            case BuffType.Force:
                forceBonus = buff.additiveBonus;
                break;
            case BuffType.Armor:
                armorBonus = buff.additiveBonus;
                break;
            default:
                break;
        }
    }
}
    void FixedUpdate()
    {
        animator.SetBool("isGrounded", groundDetection.IsGrounded);
        if (!isJumping && !groundDetection.IsGrounded)
            animator.SetTrigger("StartFall");
        isJumping = isJumping && !groundDetection.IsGrounded;
        direction = Vector3.zero; // (0,0)
        if (Input.GetKey(KeyCode.A))
            direction = Vector3.left; // (-1,0)
        if (Input.GetKey(KeyCode.D))
            direction = Vector3.right; // (1,0)
            direction *= speed;
            direction.y = rigidbody.velocity.y;
            rigidbody.velocity = direction;

        animator.SetFloat("Speed", Mathf.Abs(direction.x));

        if (direction.x>0)
        spriteRenderer.flipX = false;
        if (direction.x<0)
        spriteRenderer.flipX = true;
        CheckFall();
    }
    private void Update() {
    }
    private void CheckShoot()
    {
        if (!isReloading && groundDetection.IsGrounded)
        {       
            animator.SetTrigger("StartShoot");
        }
    }

    private void Jump()
    {
        if (groundDetection.IsGrounded)
        {
            rigidbody.AddForce(Vector2.up*(force+forceBonus), ForceMode2D.Impulse);
            //добавляем форсбонус
            animator.SetTrigger("StartJump");
            isJumping = true;
        }   
    }

    private void InitArrow()
    {
        currentArrow = GetArrowFromPool();
        currentArrow.SetImpulse
            (Vector2.right, spriteRenderer.flipX ? -force * 0.01f : force * 0.01f , this);
    }

    private void Shoot()
    {
        currentArrow.SetImpulse
            (Vector2.right, spriteRenderer.flipX ? 
                -force * shootForce : force * shootForce, this);
    }
    void CheckFall()
    {
        if (transform.position.y < minimalHeight && isCheatMode)
        {
            rigidbody.velocity = new Vector2(0,0);
            transform.position = new Vector3(0,0,0);
        }
        else if (transform.position.y < minimalHeight)
            Destroy(gameObject);
    }
    private IEnumerator ReloadingArrow()
    {
        isReloading = true;
        yield return new WaitForSeconds(reloadingTime);
        isReloading = false;
        yield break;
    }

    private Arrow GetArrowFromPool()
    {
        if (arrowPool.Count > 0)
        {
            var arrowTemp = arrowPool[0];
            arrowPool.Remove(arrowTemp);
            arrowTemp.gameObject.SetActive(true);
            arrowTemp.transform.parent = null;
            arrowTemp.transform.position = arrowSpawnPoint.transform.position;
            return arrowTemp;
        }
        return Instantiate(arrow, arrowSpawnPoint.position, Quaternion.identity);
    }
    public void ReturnArrowToPool(Arrow arrowTemp)
    {
        if (!arrowPool.Contains(arrowTemp))
        {
            arrowPool.Add(arrowTemp);
            arrowTemp.transform.parent = arrowSpawnPoint;
            arrowTemp.transform.position = arrowSpawnPoint.transform.position;
            arrowTemp.gameObject.SetActive(false);
        }
    }
}
