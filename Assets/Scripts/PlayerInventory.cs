﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour, IObjectDestroyer
{
   [SerializeField] private static PlayerInventory inventoryInstance;
   public BuffReciever buffReciever;
    public void Destroy(GameObject gameObject)
    {
        MonoBehaviour.Destroy(gameObject);
    }
    public static PlayerInventory InventoryInstance 
    { 
        get {return inventoryInstance;}
        set {inventoryInstance = value;}
    }
    private List<Item> items;
    public List<Item> Items
    {get{return items;}}

    private Item item;
    public Item Item{
        get {return item;}
    }
    [SerializeField] private int coinsCount; 
        public int CoinsCount
    {
        get {return coinsCount;}
        set {
            if(coinsCount > 0)
            coinsCount = value;
            }
    }
    [SerializeField] private Text coinsText;
    private void Start()
    {
        GameManager.Instance.inventory = this;
        coinsText.text = ""+coinsCount;
        items = new List<Item>();
    }
    void Awake() {
        inventoryInstance = this;
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if (GameManager.Instance.coinContainer.ContainsKey(other.gameObject))
        {
            coinsCount++;
            coinsText.text = "" + coinsCount;
            var coin = GameManager.Instance.coinContainer[other.gameObject];
            coin.StartDestroy();
        }

        if (GameManager.Instance.itemsContainer.ContainsKey(other.gameObject)) 
        {
            var itemComponent = GameManager.Instance.itemsContainer[other.gameObject];
            items.Add(itemComponent.Item);
            itemComponent.Destroy(other.gameObject);
        }   
    } 
}
