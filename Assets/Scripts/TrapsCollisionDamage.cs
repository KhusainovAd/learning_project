﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapsCollisionDamage : MonoBehaviour
{
    [SerializeField] private int damage = 10;
    [SerializeField] private Animator animator;
    private IObjectDestroyer destroyer;
    [SerializeField] private GameObject player;
    public int Damage
    {
        get {return damage;}
        set {
            if (value > 0 )
            damage = value;
        }
    }
    private Health health;

    private void OnCollisionStay2D(Collision2D other) 
    {
            if (GameManager.Instance.healthContainter.ContainsKey(other.gameObject))
            {   
                health = GameManager.Instance.healthContainter[other.gameObject];
                if (other.gameObject == player)
                {
                    player.GetComponent<Animator>().SetTrigger("DmgtoPlayer");
                }
            }
    }
    public void SetDamage()
    {
        if (health !=null)
            health.TakeHit(damage);
        health = null;
    }
    
}
