﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDamage : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private bool isDestroyingAfterColision;
    private IObjectDestroyer destroyer;
    private GameObject parent;
    public GameObject Parent
    {
        get {return parent; }
        set {parent = value;}
    }
    public int Damage
    {
        get { return damage; }
        set { damage = value; }
    }
    public void Init(IObjectDestroyer destroyer)
    {
        this.destroyer = destroyer;
    }
    [SerializeField] private float damageBonus;
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject == parent)
            {
                return;
            }

        if (GameManager.Instance.healthContainter.ContainsKey(other.gameObject))
        {
            var health = GameManager.Instance.healthContainter[other.gameObject];
            GameObject player = GameObject.FindWithTag("Player");//Находим плеера
            damageBonus = player.GetComponent<Player>().DamageBonus;//Вытаскиваем доп урон
            health.TakeHit(damage+(int)damageBonus);
        }
        if (isDestroyingAfterColision)
            if (destroyer == null)
                Destroy(gameObject);
            else destroyer.Destroy(gameObject);
    }
}

public interface IObjectDestroyer
{
    void Destroy(GameObject gameObject);
}